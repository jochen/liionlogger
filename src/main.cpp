#include <Arduino.h>
#include <EEPROM.h>
#include "Update.h"
#include <WiFi.h>
#include <string>  

extern "C" {
	#include "freertos/FreeRTOS.h"
	#include "freertos/timers.h"
}
//#include "ESPAsyncTCP.h"
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <AsyncMqttClient.h>

#include "soc/soc.h"`
#include "soc/rtc_cntl_reg.h"`
#include <rom/rtc.h>

//#define MQTT_HOST IPAddress(192, 168, 7, 107)
//#define MQTT_PORT 1883
//#define MQTT_TOPIC_PREFIX "test2"

int adcpin = 39; //gpio36 adc1_0
float startvbat = 0;
int last_mqtt_package_id = 0;
bool doing_update = false;
const char* version = "0.4";

AsyncMqttClient mqttClient;
//TimerHandle_t mqttReconnectTimer;
//TimerHandle_t wifiReconnectTimer;

AsyncWebServer server(80);
DNSServer dns;

extern unsigned long crc(uint8_t *blob, unsigned int size);
char get_ip[20];
char get_gw[20];
char get_sn[20];
char get_mqttHost[20];
char get_mqttPort[10];
char get_configTopic[50];

struct hostConfig
{
  IPAddress _ip;
  IPAddress _gw;
  IPAddress _sn;
  IPAddress mqttHost;
  uint16_t mqttPort;
  //char mqttUser[40];
  //char mqttPass[40];
  char configTopic[50];

  unsigned long crc;
};
hostConfig myhostConfig;
//flag for saving data
bool shouldSaveConfig = false;
//callback notifying us of the need to save config
void saveConfigCallback()
{
  Serial.println(F("Save config\n"));
  shouldSaveConfig = true;
}

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define DEFAULT_TIME_TO_SLEEP  900        /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int32_t esp_timer_time = 0;

#define Threshold 40 /* Greater the value, more the sensitivity */
touch_pad_t touchPin;

/*
Method to print the reason by which ESP32
has been awaken from sleep
*/
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}

void print_wakeup_touchpad(){
  //touch_pad_t pin;

  touchPin = esp_sleep_get_touchpad_wakeup_status();

  switch(touchPin)
  {
    case 0  : Serial.println("Touch 0 detected on GPIO 4"); break;
    case 1  : Serial.println("Touch 1 detected on GPIO 0"); break;
    case 2  : Serial.println("Touch 2 detected on GPIO 2"); break;
    case 3  : Serial.println("Touch 3 detected on GPIO 15"); break;
    case 4  : Serial.println("Touch 4 detected on GPIO 13"); break;
    case 5  : Serial.println("Touch 5 detected on GPIO 12"); break;
    case 6  : Serial.println("Touch 6 detected on GPIO 14"); break;
    case 7  : Serial.println("Touch 7 detected on GPIO 27"); break;
    case 8  : Serial.println("Touch 8 detected on GPIO 33"); break;
    case 9  : Serial.println("Touch 9 detected on GPIO 32"); break;
    default : Serial.println("Wakeup not by touchpad"); break;
  }
}

void print_reset_reason(RESET_REASON reason)
{
  switch ( reason)
  {
    case 1 : Serial.println ("POWERON_RESET");break;          /**<1, Vbat power on reset*/
    case 3 : Serial.println ("SW_RESET");break;               /**<3, Software reset digital core*/
    case 4 : Serial.println ("OWDT_RESET");break;             /**<4, Legacy watch dog reset digital core*/
    case 5 : Serial.println ("DEEPSLEEP_RESET");break;        /**<5, Deep Sleep reset digital core*/
    case 6 : Serial.println ("SDIO_RESET");break;             /**<6, Reset by SLC module, reset digital core*/
    case 7 : Serial.println ("TG0WDT_SYS_RESET");break;       /**<7, Timer Group0 Watch dog reset digital core*/
    case 8 : Serial.println ("TG1WDT_SYS_RESET");break;       /**<8, Timer Group1 Watch dog reset digital core*/
    case 9 : Serial.println ("RTCWDT_SYS_RESET");break;       /**<9, RTC Watch dog Reset digital core*/
    case 10 : Serial.println ("INTRUSION_RESET");break;       /**<10, Instrusion tested to reset CPU*/
    case 11 : Serial.println ("TGWDT_CPU_RESET");break;       /**<11, Time Group reset CPU*/
    case 12 : Serial.println ("SW_CPU_RESET");break;          /**<12, Software reset CPU*/
    case 13 : Serial.println ("RTCWDT_CPU_RESET");break;      /**<13, RTC Watch dog Reset CPU*/
    case 14 : Serial.println ("EXT_CPU_RESET");break;         /**<14, for APP CPU, reseted by PRO CPU*/
    case 15 : Serial.println ("RTCWDT_BROWN_OUT_RESET");break;/**<15, Reset when the vdd voltage is not stable*/
    case 16 : Serial.println ("RTCWDT_RTC_RESET");break;      /**<16, RTC Watch dog reset digital core and rtc module*/
    default : Serial.println ("NO_MEAN");
  }
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event) {
    Serial.printf("[WiFi-event] event: %d\n", event);
    switch(event) {
    case SYSTEM_EVENT_STA_GOT_IP:
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        connectToMqtt();
        break;
    /*case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
		    xTimerStart(wifiReconnectTimer, 0);
        break;*/
    }
}

float getADC(){

  float VBAT = (float)(analogRead(adcpin)) / 4095.0 * 2 * 3.3 * (1.1 + 0.0);
  Serial.print("Vbat = "); Serial.print(VBAT); Serial.println(" Volts");
  return VBAT;
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);

  String topicPrefix = String(myhostConfig.configTopic);

  uint16_t packetIdSub1 = mqttClient.subscribe(
    String(topicPrefix + "/firmware").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub1);
  uint16_t packetIdSub2 = mqttClient.subscribe(
    String(topicPrefix + "/topicprefix").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub2);
  uint16_t packetIdSub3 = mqttClient.subscribe(
    String(topicPrefix + "/sleeptime").c_str(), 2);
  Serial.print("Subscribing at QoS 2, packetId: ");
  Serial.println(packetIdSub3);
  /*
  mqttClient.publish("test/battery", 0, true, "test 1");
  Serial.println("Publishing at QoS 0");
  */
  float vbat = getADC();
  uint16_t packetIdPub1 = mqttClient.publish(
    String(topicPrefix + "/batterylate").c_str(), 1, true, 
    String(vbat).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub1);
  uint16_t packetIdPub2 = mqttClient.publish(
    String(topicPrefix + "/batterystart").c_str(), 1, true, 
    String(startvbat).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub2);

  uint16_t packetIdPub3 = mqttClient.publish(
    String(topicPrefix + "/bootcount").c_str(), 1, true, 
    String(bootCount).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub3);
  uint16_t packetIdPub4 = mqttClient.publish(
    String(topicPrefix + "/esp_timer_time").c_str(), 1, true, 
    String(esp_timer_time).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub4);

  uint16_t packetIdPub5 = mqttClient.publish(
    String(topicPrefix + "/info").c_str(), 1, true, 
    String(version).c_str());
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub5);

  last_mqtt_package_id = packetIdPub5;
  /*
  uint16_t packetIdPub2 = mqttClient.publish("test/lol", 2, true, "test 3");
  Serial.print("Publishing at QoS 2, packetId: ");
  Serial.println(packetIdPub2);
  */
}

void setSleepTime(uint16_t seconds) {
  esp_sleep_enable_timer_wakeup(seconds * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(seconds) +
" Seconds");
}

void touchCallback(){
  //placeholder callback function
}

void goSleep()
{
  if (doing_update) {
    return;
  }
  Serial.print("Going to sleep now. Runtime: ");
  //Setup interrupt on Touch Pad 5 (GPIO12)
  touchAttachInterrupt(T5, touchCallback, Threshold);
  //Setup interrupt on Touch Pad 6 (GPIO14)
  touchAttachInterrupt(T6, touchCallback, Threshold);
  //Configure Touchpad as wakeup source
  Serial.println((uint32_t) (esp_timer_get_time() / 1000));
  Serial.flush(); 
  esp_timer_time = (uint32_t) (esp_timer_get_time() / 1000);
  esp_deep_sleep_start();
  Serial.println("This will never be printed");
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  if (doing_update && last_mqtt_package_id == packetId) {
    String topicPrefix = String(myhostConfig.configTopic);
    uint16_t packetIdPub1 = mqttClient.publish(
      String(topicPrefix + "/firmware").c_str(), 1, true);
    Serial.print("Publishing at QoS 1, packetId: ");
    Serial.println(packetIdPub1);
    doing_update = false;
    last_mqtt_package_id = packetIdPub1;
    setSleepTime(5);
  }
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial.print("Publish received, ");
  Serial.print("  topic: ");
  Serial.print(topic);
  Serial.print(",  qos: ");
  Serial.print(properties.qos);
  Serial.print(",  dup: ");
  Serial.print(properties.dup);
  Serial.print(",  retain: ");
  Serial.print(properties.retain);
  Serial.print(",  len: ");
  Serial.print(len);
  Serial.print(",  index: ");
  Serial.print(index);
  Serial.print(",  total: ");
  Serial.print(total);
  if (len < 20) {
    Serial.print("  payload: ");
    Serial.println(payload);
  }
  else {
    Serial.println("  payload len>20");
  }

  String topicPrefix = String(myhostConfig.configTopic);


  // update requested?
  if (String(topic) == String(topicPrefix + "/firmware")) {
    doing_update = true;
    int _lastError = 0;
    // start update
    if (index == 0) {
      //Update.runAsync(true);
      if (!Update.begin(total, U_FLASH)) {
        _lastError = Update.getError();
        Update.printError(Serial);
        // ignore the rest
        topic[0] = 0;
      }
    }
    // do update in chunks
    if (!_lastError) {
      if (Update.write((uint8_t *)payload, len) != len) {
        _lastError = Update.getError();
        Update.printError(Serial);
        // ignore the rest
        topic[0] = 0;
      }
    }
    // finish update
    if (!_lastError) {
      if (index + len == total) {
        if (!Update.end()) {
          _lastError = Update.getError();
          Update.printError(Serial);
        } else {
          // restart?
          //ESP.restart();
          uint16_t packetIdUnsub1 = mqttClient.unsubscribe(
            String(topicPrefix + "/firmware").c_str());
          Serial.print("unsubcribe, packetId: ");
          Serial.println(packetIdUnsub1);
          last_mqtt_package_id = packetIdUnsub1;
        }
      }
    }
  }

  if (String(topic) == String(topicPrefix + "/sleeptime")) {
    uint16_t sleepseconds = (uint16_t) atoi(payload);
    if (sleepseconds > 3) {
      Serial.println("set new sleeptime from mqtt");
      setSleepTime(sleepseconds);
    }
  }
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  if (last_mqtt_package_id == packetId &&
    touchRead(T5) > Threshold &&
    touchRead(T6) > Threshold
  ){
  
    goSleep();
  }
}


void setup()
{
  pinMode(adcpin, INPUT);
  adcAttachPin(adcpin);
  //analogReadResolution(12);
  analogSetAttenuation(ADC_11db);

  startvbat = (float)(analogRead(adcpin)) / 4095.0 * 2 * 3.3 * (1.1 + 0.0);

  EEPROM.begin(sizeof(hostConfig) + 1);

  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.print("Version v");
  Serial.println(version);


  Serial.println("CPU0 reset reason: ");
  print_reset_reason(rtc_get_reset_reason(0));

  Serial.println("CPU1 reset reason: ");
  print_reset_reason(rtc_get_reset_reason(1));

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector  


  ++bootCount;
  Serial.println("Boot number: " + String(bootCount));

  //Print the wakeup reason for ESP32
  print_wakeup_reason();
  print_wakeup_touchpad();

  setSleepTime(DEFAULT_TIME_TO_SLEEP);
  /* esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) +
" Seconds"); */

  esp_sleep_enable_touchpad_wakeup();


  Serial.print("start Vbat = "); Serial.print(startvbat); Serial.println(" Volts");

  Serial.print("Touch 5: ");
  Serial.println(touchRead(T5));
  Serial.print("Touch 6: ");
  Serial.println(touchRead(T6));


  IPAddress _ip;
  IPAddress _gw;
  IPAddress _sn;
  IPAddress mqttHost;
  uint16_t mqttPort;
  //char mqttUser[40];
  //char mqttPass[40];
  char configTopic[50];

  AsyncWiFiManagerParameter set_ip("IP", "ESP IP", get_ip, 20);
  AsyncWiFiManagerParameter set_gw("GW", "Gateway", get_gw, 20);
  AsyncWiFiManagerParameter set_sn("NM", "Netmasq", get_sn, 20);
  AsyncWiFiManagerParameter set_mqttHost("MQTT IP", "MQTT IP", get_mqttHost, 20);
  AsyncWiFiManagerParameter set_mqttPort("MQTT Port", "MQTT PORT 1883", get_mqttPort, 10);
  AsyncWiFiManagerParameter set_configTopic("Topic", "Topic with Configuration", get_configTopic, 50);

  AsyncWiFiManager wifiManager(&server, &dns);

  wifiManager.setConfigPortalTimeout(180);
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  wifiManager.addParameter(&set_ip);
  wifiManager.addParameter(&set_gw);
  wifiManager.addParameter(&set_sn);
  wifiManager.addParameter(&set_mqttHost);
  wifiManager.addParameter(&set_mqttPort);
  wifiManager.addParameter(&set_configTopic);

  EEPROM.get(0, myhostConfig);
  if (myhostConfig.crc == crc((uint8_t *)&myhostConfig, sizeof(hostConfig)))
  {
    Serial.println(F("Valid Config readed"));
    Serial.println(myhostConfig.crc);
    
    mqttClient.onConnect(onMqttConnect);
    //mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    WiFi.onEvent(WiFiEvent);

    mqttClient.setServer(myhostConfig.mqttHost, myhostConfig.mqttPort);
    wifiManager.setSTAStaticIPConfig(myhostConfig._ip, myhostConfig._gw, myhostConfig._sn);
    //wifiManager.autoConnect(myhostConfig.hostName);
    if (!wifiManager.autoConnect("ESPButtonConfig")) {
      Serial.println("failed to connect and hit timeout");
      goSleep();
    }

  }
  else
  {
    Serial.println(F("Invalid Config readed"));
    Serial.println(myhostConfig.crc);
    wifiManager.resetSettings();
    if (!wifiManager.startConfigPortal("OnDemandAP")) {
      Serial.println("failed to connect and hit timeout");
      delay(3000);
      //reset and try again, or maybe put it to deep sleep
      setSleepTime(10);
      goSleep();
    }
    //wifiManager.autoConnect(myhostConfig.hostName);
  }


  if (shouldSaveConfig)
  {
    myhostConfig._ip.fromString(String(set_ip.getValue()));
    myhostConfig._gw.fromString(String(set_gw.getValue()));
    myhostConfig._sn.fromString(String(set_sn.getValue()));
    myhostConfig.mqttHost.fromString(String(set_mqttHost.getValue()));
    myhostConfig.mqttPort = atoi(set_mqttPort.getValue());
    strcpy(myhostConfig.configTopic, set_configTopic.getValue());

    myhostConfig.crc = crc((uint8_t *)&myhostConfig, sizeof(hostConfig));
    EEPROM.put(0, myhostConfig);
    EEPROM.commit();
    Serial.println(myhostConfig.crc);
    setSleepTime(10);
    goSleep();
  }

  /*
  WiFi.config(local_IP, gateway, subnet, primaryDNS);

  Serial.print("Connecting to ");
  Serial.println(ssid);

  //WiFi.mode(WIFI_STA);
  //WiFi.enableSTA(true);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected!");
  */
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  //Serial.print("DNS: ");
  //Serial.println(WiFi.dnsIP());


}

void loop()
{
  Serial.print("Touch (loop) 5: ");
  Serial.println(touchRead(T5));
  Serial.print("Touch (loop) 6: ");
  Serial.println(touchRead(T6));
  delay(30000);

  //Serial.print("connecting to ");

  // Use WiFiClient class to create TCP connections
  //WiFiClient client;

  //Serial.println();
  //Serial.println("closing connection");
  Serial.println("Timeout, go to sleep");
  goSleep();
}

